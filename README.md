# SDC HACK - Orios

Voici notre projet de code camp Hack, celui ci à pour but : le Développement d'un scanner de vulnérabilité pour site web

## Getting Started

Pour commencer vous allez devoir recupérer le projet via le git lab ETNA

Exemple: 

```
git clone "https://rendu-git.etna-alternance.net/module-5750/activity-31913/group-702113.git"
```

### Prerequisites

Pour lancement du projet, il va vous falloir : 

#### Python 3.6

Pour windows et Mac, rendez vous sur le site de python

https://www.python.org/downloads/release/python-373/

Pour linux Ubuntu, voici un tutorial pour l'installation 

https://linuxize.com/post/how-to-install-python-3-7-on-ubuntu-18-04/

#### Wkhtmltopdf

Pour la gestion du pdf il vous faut télécharger Wkhtmltopdf via ce lien: 

https://wkhtmltopdf.org/

Et changer le chemin absolue de la config dans pdf.py

### Installing

##### Modules Python

Pour l'installation des modules python, un fichier txt sous le nom de requirements.txt est là pour vous aider

 ```
pip install -r requirements.txt
 ```

## Running the script

Pour le lancement du script, vous devez vous placer à la racine du dossier et effectuer cette commande:

 ```
python3 main.py 
 ```
 
## Check the result

Pour voir le résultat du scanneur, il vous suffit de lancer le pdf "result.pdf" qui se situe dans le dossier result

## Authors

* **Nourredine Nassiri** 
* **Wadi Benarbia** 
* **Robin Klein** 

