import requests

class PHP:
    result = requests.get(targetURL, data)
    json= result.headers

    #Get le PHPSESSID des cookie
    def cookiePHP(json):
        if json['Set-Cookie']:
            id = json['Set-Cookie']
            phpid = id.split(';')
            session = phpid[0].split('=')
            return session[1]   
        return False
    
    id = cookiePHP(json)

    def msgToPDF(id):
        if id is not None and id:
            return "Le PHPSESSID n'est pas hashé, il y a un rique !"
        else: 
            return "Pas de token trouvé"